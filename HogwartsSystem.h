//Just check if this class decleared before
#ifndef HOGWARTSSYSTEM_H
#define HOGWARTSSYSTEM_H

//Needed for store students datas
#include <vector>

//Needed for output and input streams
#include <iostream>

//Needed for store courses datas
#include "Course.h"

//Needed for store birthday
#include "Date.h"

//Needed for store students datas
#include "Student.h"

//Some good functions to do some string operation
#include "StringUtility.h"

//Ease of use
using namespace std;

const int COURSESCOUNT = 5;

class HogwartsSystem {
    private:

    //We have 5 courses provided this mid-term at Hogwarts
    Course courses[COURSESCOUNT]; 
    vector <Student> students;

    public:

    int getCourseIndex(string id);

    //After completely and correctly added a student we can calculate number of units
    int calculateNumberOfTakenUnits(string snn);

    /*Add wanted courses for given snn
    Each course given by its id*/
    void addCoursesToStudent();

    //Add course datas in constructor initializer list
    HogwartsSystem() :courses{
        Course("DarkArt", 3, "1256", "Snape"), 
        Course("Fly", 1, "1331", "McGonagall"),
        Course("Potions", 2, "1259", "Slughorn"),
        Course("Divination", 3, "6587", "Firenze"),
        Course("Herbology", 2, "1578", "Sprout")} {
    }

    //Just for appearance
    void printHorizontalLine();

    Student* getStudent(string snn);

    //Each input stream and response goes through here
    void mainLoop();

    //Register a student
    void registeration();

    //View student's data
    void viewStudent();

    //View informations of a course
    void viewCourse();
};

int HogwartsSystem::calculateNumberOfTakenUnits(string snn) {
    vector <Course> takenCourses = getStudent(snn) -> getCourses();
    int ans = 0;
    //Sum unit value of each course
    for (int i = 0; i < takenCourses.size(); i++) {
        ans += takenCourses[i].getUnitNumber();
    }
    return ans;
}

Student* HogwartsSystem::getStudent(string snn) {
    for (int i = 0; i < students.size(); i++) {
        if (students[i].getSnn() == snn) {
            return &students[i];
        }
    }
    return NULL;
}

int HogwartsSystem::getCourseIndex(string id) {
    for (int i = 0; i < COURSESCOUNT; i++) {
        if (courses[i].getId() == id) {
            return i;
        }
    }
    return -1;
}

void HogwartsSystem::registeration() {

    //We hired this object variable to do our string operations
    StringUtility stringEmployee;

    printHorizontalLine();
    cout << "Registeration process:\n\n";

    cout << "Enter number of students wanna register: ";
    //Check for preventing bad input
    string nString;
    cin >> nString;
    if (!stringEmployee.isAllDigit(nString)) {
        cout << "Why your input wasn't an integer :(\nlets try again with valid input types please\n";
        return;
    }
    int n = stringEmployee.convertStringToNumber(nString);

    //This loop runs n time for each student
    for (int i = 0; i < n; i++) {

        string name, family;
        cout << "Enter name of student:";
        cin >> name;
        cout << "Enter family of student: ";
        cin >> family;

        //Default value of birthday should be 1998,1,1 as explained in the statement
        Date birthday(1998, 1, 1);
        cout << "Enter Birthday:(just enter 0 if don't have this info) ";
        //>> operator overloaded before in Date.h header file
        cin >> birthday;

        cout << "Enter snn: ";
        string snn;
        cin >> snn;

        //snn should be an alldigit string
        if (!stringEmployee.isAllDigit(snn)) {
            cout << "Registration Failed.(Entered snn is not a valid snn)\n";
            return;
        }

        //Any student is allowed to register just once
        if (getStudent(snn) != NULL) {
            cout << "Registration Failed.(This student was registered before.)\n";
            return;
        }

        //Check for validity of given information
        if (birthday.isValid() && stringEmployee.isAllDigit(snn) && 
        stringEmployee.isAllLetters(name) && stringEmployee.isAllLetters(family)) {
            //add student to database passing by an object
            students.push_back(Student(name, family, birthday, snn));

            cout << name << " " << family << " with snn:" << snn << " Successfully Registered.\n";
        } else {
            cout << "Registration Failed.\n";
        }
    }
}

void HogwartsSystem::printHorizontalLine() {
    for (int i = 0; i < 50; i++) {
        cout << '#';
    } cout << '\n';
}

void HogwartsSystem::addCoursesToStudent() {

    //We hired this object variable to do our string operations
    StringUtility stringEmployee;

    cout << "Take courses process:\n\n";
    
    cout << "Enter student's snn: ";
    string snn;
    cin >> snn;

    //snn should be an alldigit string
    if (!stringEmployee.isAllDigit(snn)) {
        cout << "Take courses Failed.(Entered snn is not a valid snn)\n";
        return;
    }

    //Find given student among students of our database
    Student* stu = getStudent(snn);

    cout << "Enter number of courses the student wanna take: ";
    //Check for preventing bad input
    string nString;
    cin >> nString;
    if (!stringEmployee.isAllDigit(nString)) {
        cout << "Why your input wasn't an integer :(\nlets try again with valid input types please\n";
        return;
    }
    int n = stringEmployee.convertStringToNumber(nString);

    //We store wanted courses here in this vector
    vector <Course> ans;

    //Later we use this variable as result of checking validations of take courses process
    bool failure = false;

    //Below loop repeats n times for each course
    for (int i = 0; i < n; i++) {
        cout << "Enter Id of wanted course: ";
        string id;
        cin >> id;

        //Find given course among courses of our database
        int courseIndex = getCourseIndex(id);
        if (courseIndex == -1) {
            failure = true;
        } else {
            //Check if this student took the course before
            bool found = false;
            for (int i = 0; i < ans.size(); i++) {
                if (ans[i].getId() == courses[courseIndex].getId()) {
                    found = true;
                }
            }

            if (!found) {
                ans.push_back(courses[courseIndex]);
            }
        }
    }

    //In case of unregistered student
    if (stu == NULL) {
        cout << "Given student is not registered\n";
        return;
    }

    if (stu -> tookCoursesBefore()) {
        cout << "Sorry, this student did the process before\n";
        return;
    }

    //Add courses to student object
    stu -> addCourses(ans);

    //Check for valid number of taken units
    int unitSum = calculateNumberOfTakenUnits(snn);
    if (unitSum <= 8 && unitSum >= 3) {
        cout << stu -> getNameFamily() <<  " with snn:" << snn << " got " << unitSum << " unit Course.\n";
    } else {
        cout << "There are some problems.(sum of taken units is not valid)\n";
    }
}

void HogwartsSystem::viewStudent() {
    //We hired this object variable to do our string operations
    StringUtility stringEmployee;

    cout << "View student informations process:\n\n";
    
    cout << "Enter student's snn: ";
    string snn;
    cin >> snn;
    
    //snn should be an alldigit string
    if (!stringEmployee.isAllDigit(snn)) {
        cout << "View student Failed.(Entered snn is not a valid snn)\n";
        return;
    }

    Student* stu = getStudent(snn);

    //In case of unregistered
    if (stu == NULL) {
        cout << "Given snn is unregistered in our database\n";
        return;
    }

    cout << stu -> getNameFamily() << " with " << stu -> getBirthday() -> getAge() << " years old and snn:" << stu -> getSnn();
    cout << " got these courses:\n";

    //Repeats for each course taken
    for (int i = 0; i < stu -> getCourses().size(); i++) {
        cout << (stu -> getCourses())[i].getName() << '\n';
    }
}

void HogwartsSystem::viewCourse() {
    //We hired this object variable to do our string operations
    StringUtility stringEmployee;

    cout << "View course process:\n\n";
    
    cout << "Enter id of course: ";
    string id;
    cin >> id;
    
    //snn should be an alldigit string
    if (!stringEmployee.isAllDigit(id)) {
        cout << "View course Failed.(Entered id is not a valid id)\n";
        return;
    }

    //Find the course and show name of the course to user
    string courseName, courseTeacher;
    for (int i = 0; i < COURSESCOUNT; i++) {
        if (courses[i].getId() == id) {
            courseName = courses[i].getName();
            courseTeacher = courses[i].getTeacher();
        }
    }

    //Not found any course with given id
    if (!courseName.size()) {
        cout << "There is not any course with given id\n";
        return;
    }

    cout << "Students that Got " << courseName << "  with id:" << id << " and teacher:" << courseTeacher << ":\n";
    //Show student list
    for (int i = 0; i < students.size(); i++) {
        if (students[i].hasCourse(id)) {
            cout << students[i].getNameFamily() << " with snn:" << students[i].getSnn() << '\n';
        }
    }
}

void HogwartsSystem::mainLoop() {

    bool running = true;
    //Untill user wanna do something with the system this loop should run
    while (running) {
        
        printHorizontalLine();

        //User guide
        cout << "For student registeration enter 1\n\n";
        cout << "For add courses to a specific student enter 2\n\n";
        cout << "For view a specific student informations enter 3\n\n";
        cout << "For view list of students of a specific course enter 4\n\n";
        cout << "For exit enter 0\n";

        printHorizontalLine();
        
        //User guide
        cout << "Which operation you wanna do? ";
        
        //Input user's option
        char c;
        cin >> c;

        switch (c) {
            case '1':
            registeration();
            break;

            case '2':
            addCoursesToStudent();
            break;

            case '3':
            viewStudent();
            break;

            case '4':
            viewCourse();
            break;

            case '0':
            cout << "Successfuly terminated the program!";
            //Changing this flag to false make the mainloop stopped
            running = false;
            break;

            //In case of bad input
            default:
            cout << "Just enter number of option listed above, not anything else\n";
        }
    }
}

#endif