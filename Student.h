//Just check if this class decleared before
#ifndef STUDENT_H
#define STUDENT_H

//Needed for store snn, family and name
#include <string>

//Needed for store courses of each student object
#include "Course.h"

//Needed for set any Date type value
#include "Date.h"

class Student {
    private:
    string snn, family, name;
    Date birthday;
    vector <Course> courses;

    public:
    /*Default value of birthday should be 1998,1,1 as explained in the statement
    Construct student object using given information from user*/
    Student(string name, string family, Date bd, string snn) : birthday(bd) {
        this -> name = name;
        this -> family = family;
        this -> snn = snn;
    }

    //Not allowed to do the process more than once
    bool tookCoursesBefore();

    Date* getBirthday();

    //Use snn later for finding a specific student
    string getSnn();

    vector <Course> getCourses();

    string getNameFamily();

    //Determine if this student has a specific course or not
    bool hasCourse(string id);

    //Just copy given courses list to student object storage
    void addCourses(vector <Course> given);
};

bool Student::tookCoursesBefore() {
    return courses.size() > 0;
}

bool Student::hasCourse(string id) {
    for (int i = 0; i < courses.size(); i++) {
        if (courses[i].getId() == id) {
            return true;
        }
    }
    return false;
}

Date* Student::getBirthday() {
    return &birthday;
}

string Student::getNameFamily() {
    return name + ' ' + family;
}

vector <Course> Student::getCourses() {
    return courses;
}

void Student::addCourses(vector <Course> given) {
    courses = given;
}

string Student::getSnn() {
    return snn;
}

#endif