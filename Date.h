//Just check if this class decleared before
#ifndef DATE_H
#define DATE_H

//Some good functions to do some string operation
#include "StringUtility.h"

class Date {
    //Allow access because of that later in validation of user's input we need aid of >> operator
    friend istream& operator>> (istream& input, Date& birthday); 

    private:
    int year, month, day;

    //Later for validation of user's input date
    bool valid;
    void setInvalid();

    public:
    Date(int year, int month, int day) { 
        //Construct the object
        this -> year = year;
        this -> month = month;
        this -> day = day;
        valid = year >= 0 && year <= 2020 && month >= 1 && month <= 12 && day >= 1 && day <= 31;
    }

    //Return validity of the given date
    bool isValid();

    //Calculate age of the student
    int getAge();
};

bool Date::isValid() {
    return valid;
}

int Date::getAge() {
    return 2020 - year;
}

void Date::setInvalid() {
    valid = false;
}

//This operator helps us take input better and prettier
istream& operator>> (istream& input, Date& birthday) {

    //We hired this object variable to do our string operations
    StringUtility stringEmployee;

    string s;
    input >> s;

    //In case of 0
    if (s == "0") {
        birthday = Date(1998, 1, 1);
        return input;
    }

    //For store splitted not yet integered year, month and day number
    string splitted[3];

    //Splitting
    int i = 0, splitterCount = 0;
    for (; i < s.size(); i++) {

            if (s[i] < '0' || s[i] > '9') { //Non-digits

                //Case of bad splitter
                if (s[i] != '/') {
                    birthday.setInvalid();
                    return input;
                } else { //Case of "/"
                    splitterCount++;
                    //Case of more than three splitter
                    if (splitterCount > 2) {
                        birthday.setInvalid();
                        return input;
                    }
                }
            } else { //Digits
                splitted[splitterCount] += s[i];
            } 
    }

    if (splitterCount < 2) {
        birthday.setInvalid();
        return input;
    }

    //Split year number from rest of input line
    birthday = Date(
        stringEmployee.convertStringToNumber(splitted[0]), 
        stringEmployee.convertStringToNumber(splitted[1]), 
        stringEmployee.convertStringToNumber(splitted[2]));
    return input;
}

#endif