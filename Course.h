//Just check if this class decleared before
#ifndef COURSE_H
#define COURSE_H

//Needed for store name, id and teacher's name
#include <string>

//Ease of use
using namespace std;

class Course {
    //Voldemort can not access these datas
    private:
    string name, id, teacher;
    int unitNumber;

    public:
    //Set amounts of the object
    Course(string name, int unitNumber, string id, string teacher){
        this -> name = name;
        this -> unitNumber = unitNumber;
        this -> id = id;
        this -> teacher = teacher;
    }

    int getUnitNumber();

    //Access to Id
    string getId();

    string getName();

    string getTeacher();
};

string Course::getTeacher() {
    return teacher;
}

string Course::getName() {
    return name;
}

int Course::getUnitNumber() {
    return unitNumber;
}

string Course::getId() {
    return id;
}

#endif