//Just check if this class decleared before
#ifndef STRINGUTILITY_H
#define STRINGUTILITY_H

class StringUtility {
    public:

    //Change type of given string to integer
    int convertStringToNumber(string s);

    //Determine all characters of given string are digit or not
    bool isAllDigit(string s);

    //Determine all characters of given string are letter/space or not
    bool isAllLetters(string s);
};

int StringUtility::convertStringToNumber(string s) {
    int n = s.size(), ans = 0;
    for (int i = n - 1, j = 1; i >= 0; i--, j *= 10) {
        ans += (s[i] - '0') * j;
    }
    return ans;
}

bool StringUtility::isAllDigit(string s) {
    for (int i = 0; i < s.size(); i++) {
        if (s[i] < '0' || s[i] > '9') {
            return false;
        }
    }
    return s.size();
}

bool StringUtility::isAllLetters(string s) {
    bool ans = true;
    for (int i = 0; i < s.size(); i++) {
        ans &= s[i] <= 'Z' && s[i] >= 'A' || s[i] <= 'z' && s[i] >= 'a' || s[i] == ' ';
    }
    return ans;
}

#endif